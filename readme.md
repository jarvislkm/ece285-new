# Description 
 
This is VAE project developed by team __LLLS__ composed of Kaiming Liu, Yijing Li, Saina Shawulieti and Yizhi Li

# Code organization 

Our codes are all in the ```VAE_Project``` folder

* Model -- Including 1 and 2 layers model  
        * ```vae.py```  
        * ```vae_2layer.py```  
* Utils -- Functions called in run.py  
	    * ```cache.py``` Save record of loss and likelihood  
	    * ```evaluation.py```  
	    * ```latentcluster.py``` mapping training data into latent space and visualization  
	    * ```likelihood.py```   
	    * ```loadData.py```   
	    * ```sampling.py``` randomly sampling on latent space to generate new image  
	    * ```training.py```  
	    * ```unisampling.py``` uniformly sampling on 2d latent space  
* datasets -- Incudling multiple dataset that can be loaded in ```loadData.py```  
* result -- Generated by ```run.py```  
* result_demo -- Generated by ```run_demo.py```  
* ```run.py```-- Main code. A user-friendly interfaces. Any parameter can be adjust here.   
* ```run_demo.py```-- Demo code. A copy of ```run.py``` with chosen parameters.  
* ```demo.ipynb```-- __Please run this on DSMPL.__ It will call the run_demo.py and show the results.   

# Demo  
Please run the ```demo.ipynb``` on __GPU__ and all the results will show in the jupyter notebook.  

#Others
old repo in https://bitbucket.org/jarvislkm/ece285/src, which is too large. So we use this new one to submit.

import pickle
import numpy as np
import matplotlib.pyplot as plt
with open('./2018-06-15-16-15-11_base1.pickle', 'rb') as f:
    x = pickle.load(f)
with open('./2018-06-15-16-18-46_100_1.pickle', 'rb') as g:
    y = pickle.load(g)
with open('./2018-06-15-16-27-03_150_1.pickle', 'rb') as h:
    z = pickle.load(h)
with open('./2018-06-15-16-28-20_200_1.pickle', 'rb') as m:
    a = pickle.load(m)
with open('./2018-06-15-16-35-12_50_1.pickle', 'rb') as n:
    b = pickle.load(n)
with open('./2018-06-15-16-41-50_30_1.pickle', 'rb') as o:
    c = pickle.load(o)
with open('./2018-06-15-16-51-46_10_1.pickle', 'rb') as p:
    d = pickle.load(p)
with open('./2018-06-15-16-56-24_20_1.pickle', 'rb') as q:
    e = pickle.load(q)


data = np.array(x)
print(data.shape)
trainRecord = np.array(data[0,:])
evalRecord = np.array(data[1,:])
testRecord = np.array(data[2,:])
likelihood1 = -1*np.array(data[3,:])

# train_loss = []
# eval_loss = []
# test_loss = []
# for i in range(trainRecord.shape[0]):
#     loss_1 = trainRecord[i][0]
#     loss_2 = evalRecord[i][0]
#     loss_3 = testRecord[i][0]
#     train_loss.append(loss_1)
#     eval_loss.append(loss_2)
#     test_loss.append(loss_3)

data2 = np.array(y)

trainRecord2 = np.array(data2[0,:])
evalRecord2 = np.array(data2[1,:])
testRecord2 = np.array(data2[2,:])
likelihood2 = -1*np.array(data2[3,:])

data3 = np.array(z)

trainRecord3 = np.array(data3[0,:])
evalRecord3 = np.array(data3[1,:])
testRecord3 = np.array(data3[2,:])
likelihood3 = -1*np.array(data3[3,:])

data4 = np.array(a)
trainRecord4 = np.array(data4[0,:])
evalRecord4 = np.array(data4[1,:])
testRecord4 = np.array(data4[2,:])
likelihood4 = -1*np.array(data4[3,:])

data5 = np.array(b)
trainRecord5 = np.array(data5[0,:])
evalRecord5 = np.array(data5[1,:])
testRecord5 = np.array(data5[2,:])
likelihood5 = -1*np.array(data5[3,:])

data6 = np.array(c)
trainRecord6 = np.array(data6[0,:])
evalRecord6 = np.array(data6[1,:])
testRecord6 = np.array(data6[2,:])
likelihood6 = -1*np.array(data6[3,:])

data7 = np.array(d)
trainRecord7 = np.array(data7[0,:])
evalRecord7 = np.array(data7[1,:])
testRecord7 = np.array(data7[2,:])
likelihood7 = -1*np.array(data7[3,:])

data8 = np.array(e)
trainRecord8 = np.array(data8[0,:])
evalRecord8 = np.array(data8[1,:])
testRecord8 = np.array(data8[2,:])
likelihood8 = -1*np.array(data8[3,:])


n2 = trainRecord.shape[0]
# plt.plot(range(0,n2), train_loss, label="train")
# plt.plot(range(0,n2), eval_loss, label ="eval")
# plt.plot(range(0,n2), test_loss, label="test")
plt.plot(range(0,n2), likelihood1, label="No PCA", linewidth=3.5)
plt.plot(range(0,n2), likelihood3, label="PCA Dimension = 150")
plt.plot(range(0,n2), likelihood2, label="PCA Dimension = 100")
# plt.plot(range(0,n2), likelihood4, label="likelihood_200")
plt.plot(range(0,n2), likelihood5, label="PCA Dimension = 50")
plt.plot(range(0,n2), likelihood6, label="PCA Dimension = 30", linewidth=3.5)
# plt.plot(range(0,n2), likelihood7, label="likelihood_10")
plt.plot(range(0,n2), likelihood8, label="PCA Dimension = 20")

plt.xlabel('epoch')
plt.ylabel('likelihood')
plt.title('MNIST Likelihood Comparison')
plt.legend()

plt.show()


# plt.plot([i for i in range(0,n2)], train_lst, label="train_0.5e-3")
# plt.plot([i for i in range(0,n2)], test_lst, label="test_0.5e-3")
# plt.plot([i for i in range(0,n2)], train_lst2, label="train_1e-3")
# plt.plot([i for i in range(0,n2)], test_lst2, label="test_1e-3")
# # plt.plot([i for i in range(0,n2)], train_lst3, label="train_hidden200")
# # plt.plot([i for i in range(0,n2)], test_lst3, label="test_hidden200")
# plt.xlabel('epoch')
# plt.ylabel('loss')
# plt.title('Loss_CIFAR 10_diff learning rate (Hidden 120) ')
# # plt.xlim((15,30))
# plt.ylim((1500,3000))
# plt.legend()

# plt.show()
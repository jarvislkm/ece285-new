import pickle
import numpy as np
import matplotlib.pyplot as plt
with open('./2018-06-15-12-24-03.pickle', 'rb') as f:
    x = pickle.load(f)
with open('./2018-06-15-12-24-03.pickle', 'rb') as g:
    y = pickle.load(g)
with open('./2018-06-15-12-24-03.pickle', 'rb') as h:
    z = pickle.load(h)


data = np.array(x)
print(data.shape)
trainRecord = np.array(data[0])
evalRecord = np.array(data[1])
testRecord = np.array(data[2])
likelihoodRecord = np.array(data[3])
train_lst = trainRecord
eval_lst = evalRecord
test_lst = testRecord
likelihood_lst = likelihoodRecord
print(trainRecord.shape)

# data2 = np.array(y)

# trainRecord2 = np.array(data2[0])
# evalRecord2 = np.array(data2[1])
# testRecord2 = np.array(data2[2])
# likelihoodRecord2 = np.array(data[3])
# train_lst2 = trainRecord2[:,0]
# eval_lst2 = evalRecord2[:,0]
# test_lst2 = testRecord2[:,0]
# likelihood_lst2 = likelihoodRecord[:,0]

# data3 = np.array(z)

# trainRecord3 = np.array(data3[0])
# evalRecord3 = np.array(data3[1])
# testRecord3 = np.array(data3[2])
# likelihoodRecord3 = np.array(data[3])
# train_lst3 = trainRecord3[:,0]
# eval_lst3 = evalRecord3[:,0]
# test_lst3 = testRecord3[:,0]
# likelihood_lst3 = likelihoodRecord[:,0]


n2 = train_lst.shape[0]

plt.plot([i for i in range(0,n2)], train_lst, label="train")
plt.plot([i for i in range(0,n2)], eval_lst, label ="eval")
plt.plot([i for i in range(0,n2)], test_lst, label="test")
plt.xlabel('epoch')
plt.ylabel('loss')
# plt.ylim((1500,3000))
plt.title('Loss_CIFAR_10_Hidden 120_epoch70_lr0.5e-3')
plt.legend()

plt.show()


plt.plot([i for i in range(0,n2)], train_lst, label="train_0.5e-3")
plt.plot([i for i in range(0,n2)], test_lst, label="test_0.5e-3")
plt.plot([i for i in range(0,n2)], train_lst2, label="train_1e-3")
plt.plot([i for i in range(0,n2)], test_lst2, label="test_1e-3")
# plt.plot([i for i in range(0,n2)], train_lst3, label="train_hidden200")
# plt.plot([i for i in range(0,n2)], test_lst3, label="test_hidden200")
plt.xlabel('epoch')
plt.ylabel('loss')
plt.title('Loss_CIFAR 10_diff learning rate (Hidden 120) ')
# plt.xlim((15,30))
# plt.ylim((1500,3000))
plt.legend()

plt.show()
import argparse
import torch
from torch import optim
from Utils.training import train, likelihood
from Utils.evaluation import evaluation, testing
from Utils.sampling import sampling, cvae_create_target_one_hot, cvae_create_target_line
from Utils.unisampling import unisampling
import Utils.loadData as loadData
from Utils.cache import saveCache
import datetime
from Utils.latentcluster import latentcluster3D,latentcluster2D


parser = argparse.ArgumentParser(description='VAE Experiment')

# Model hyperparameters:
parser.add_argument('--model', type=str, default='vae', metavar='N',
                    help='vae, vae_2layer')
parser.add_argument('--hiddenSize', type=int, default=2, metavar='N',
                    help='number of latent variables')
parser.add_argument('--ILD', type=int, default=400, metavar='N',
                    help='internal layer dimension')
parser.add_argument('--KLratio', type=float, default=1.0, metavar='N',
                    help='ratio between RE & KL in loss function')

# Datasets:
parser.add_argument('--dataSet', type=str, default='MNIST', metavar='N',
                    help='MNIST, OMNIGLOT, StreetNumber')
    # For Street number dataset: grey or RGB
parser.add_argument('--grey', type=bool, default=False, metavar='N',
                    help='if use grey scale or not')

# PCA Pre-Processing settings:
parser.add_argument('--ifpca', type=bool, default=False, metavar='N',
                    help='pca or not')
parser.add_argument('--pcadimension', type=int, default=1000, metavar='N',
                    help='pca K value')
    # Experience KLratio / pcadimension for different datasets when ifpca == True:
    # MNIST       1.0    /     ~300
    # OMNIGLOT    0.2    /     ~300
    # Street      1.0(not sure)     /     ~1000

# Conditional VAE settings:
parser.add_argument('--cvae', type=bool, default=False, metavar='N',
                    help='conditional vae')
parser.add_argument('--cvae_select', type=str, default='number', metavar='N',
                    help='number, line')

# Training hyperparameters:
parser.add_argument('--batch-size', type=int, default=512, metavar='N',
                    help='input batch size for training')
parser.add_argument('--epochs', type=int, default=10, metavar='N',
                    help='number of epochs to train')
    # if cost = nan, please reduce lr
    # most case 1e-3 is ok; for street number, maybe need 1e-4
parser.add_argument('--lr', type=float, default=3e-3, metavar='N',
                    help='learning rate')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='enables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed')

# Saving:
parser.add_argument('--log-interval', type=int, default=30, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--saveInterval', type=int, default=1, metavar='N',
                    help='how many epoch to save test image')

args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()
args.path = 'result_demo'
args.device = torch.device("cuda" if args.cuda else "cpu")
args.conditional_dim = 0
args.mu = 0
args.U = []

kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}
args, train_loader, val_loader, test_loader = loadData.load_data(args, **kwargs)

if args.model == 'vae':
    from Models.vae import vae
elif args.model == 'vae_2layer':
    from Models.vae_2layer import vae

print(args)

model = vae(args).to(args.device)
optimizer = optim.Adam(model.parameters(), lr=args.lr)

trainRecord = []
evalRecord = []
testRecord = []
likelihoodRecord = []

for epoch in range(1, args.epochs + 1):
    train_loss, train_KL, train_RE = train(model, train_loader, epoch, optimizer, args)
    eval_loss, eval_KL, eval_RE = evaluation(model, val_loader, epoch, optimizer, args)
    test_loss, test_KL, test_RE = testing(model, test_loader, epoch, optimizer, args)
    trainRecord.append([train_loss, train_KL, train_RE])
    evalRecord.append([eval_loss, eval_KL, eval_RE])
    testRecord.append([test_loss, test_KL, test_RE])

    # calculate likelihood using importance sampling
    # Likelihood is what we compare between different settings!
    
    # likeli = likelihood(model, test_loader, epoch, args)
    # likelihoodRecord.append(likeli)

    cvae_target = torch.tensor([])
    if args.cvae == True:
        if args.cvae_select == 'number':
            cvae_target = cvae_create_target_one_hot(args, 64, 4)
        elif args.cvae_select == 'line':
            cvae_target = cvae_create_target_line(args, 8, test_loader).numpy() 
            cvae_target = torch.tensor(cvae_target.repeat(8,0))     

    sampling(64, cvae_target, model, epoch, args)

    if args.cvae == False:
        if args.hiddenSize == 2:
            unisampling(15, model, epoch, args)

if args.model == 'vae':
    if args.hiddenSize == 3:
        latentcluster3D(model, val_loader, args)
    elif args.hiddenSize == 2:
        latentcluster2D(model, val_loader, args)

lossRecord = [trainRecord, evalRecord, testRecord, likelihoodRecord]
msg = saveCache(lossRecord, args)
print('Saved record:' + msg)

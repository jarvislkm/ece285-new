import torch
from torchvision.utils import save_image
import pathlib
from Utils.loadData import pca_decode

def evaluation(model, val_loader, epoch, optimizer, args):
    eval_loss, eval_KL, eval_RE = eval_kernel(model, val_loader, epoch, optimizer, args, 'Eval')
    return eval_loss, eval_KL, eval_RE
def testing(model, test_loader, epoch, optimizer, args):
    test_loss, test_KL, test_RE = eval_kernel(model, test_loader, epoch, optimizer, args, 'Test')
    return test_loss, test_KL, test_RE

def eval_kernel(model, data_loader, epoch, optimizer, args, sign):
    model.eval()
    test_loss = 0
    test_KL = 0
    test_RE = 0
    #Test do not need to update weight
    with torch.no_grad():
        for batch_index, (data, labels) in enumerate(data_loader):
            data = data.to(args.device)
            labels = labels.to(args.device)
            loss, RE, KL, recon = model.loss(data, labels)
            test_loss += loss.item()
            test_RE += RE.item()
            test_KL += KL.item()
            if args.ifpca == True:
                decode_recon = pca_decode(args.device, recon, args.mu, args.U)
                decode_data = pca_decode(args.device, data, args.mu, args.U)
            else:
                decode_recon = recon
                decode_data = data

            if epoch % args.saveInterval == 0 and batch_index == 0:
                number = min(8, decode_data.shape[0])
                combination = torch.cat([decode_data[:number],decode_recon[:number]])
                savePath = args.path + '/' + sign
                pathlib.Path(savePath).mkdir(parents=True,exist_ok=True)
                save_image(combination.view(number*2,args.inputSize[0], args.inputSize[1], args.inputSize[2]).cpu(),
                           savePath+'/' +str(epoch)+'.png', nrow = number)

    test_loss /= (len(data_loader))
    test_RE /= (len(data_loader))
    test_KL /= (len(data_loader))
    print('====> '+ sign +' set loss: {:.4f}'.format(test_loss))
    return test_loss, test_KL, test_RE


import torch
import numpy as np
from Utils.likelihood import logsumexp

def train(model, train_loader, epoch, optimizer, args):
    train_loss = 0
    train_RE = 0
    train_KL = 0
    #Turn to train setting
    model.train()
    for batch_index, (data, labels) in enumerate(train_loader):
        #For each epoch, clean gradient at first
        optimizer.zero_grad()
        data = data.to(args.device)
        labels = labels.to(args.device)
        Loss, RE, KL, recon = model.loss(data, labels)
        Loss.backward()
        train_loss += Loss.item()
        train_RE += RE.item()
        train_KL += KL.item()
        optimizer.step()
        if batch_index % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\t Loss: {:.6f}'.format(
                epoch, batch_index*len(data), len(train_loader)*len(data),
                100.*batch_index / len(train_loader),
                Loss.item()))
    train_loss /= (len(train_loader))
    train_RE /= (len(train_loader))
    train_KL /= (len(train_loader))
    print('===> Epoch:{} Average loss: {:.4f}'.format(epoch, train_loss))
    return train_loss, train_KL, train_RE

# importance sampling method for marginal likelihood
def likelihood(model, data_loader, epoch, args):
    likelihoodRecord = []
    randomTimes = 10
    for batch_index, (data, labels) in enumerate(data_loader):
        data = data.to(args.device)
        if batch_index % 10 == 0:
            print('likelihood: {:.2f}%'.format(batch_index / (1. * len(data_loader)) * 100))
        likeli_tmp = []
        model.train()
        # Generate several z for same x, find the best one with lowest loss.
        for s in range(randomTimes):
            Loss, RE, KL, _ = model.loss(data, labels, False)
            likeli_tmp.append((-Loss.cpu().data.numpy()).tolist())

        likeli_tmp = np.asarray(likeli_tmp)
        # Marginal likelihood
        likeli = np.mean(logsumexp(likeli_tmp) - np.log(len(likeli_tmp)))
        likelihoodRecord.append(likeli)

    likelihoodRecord = np.array(likelihoodRecord)
    likeli = np.mean(likelihoodRecord)
    print('likelihood at eopch: ' + str(epoch) + ' {:.2f}'.format(likeli))
    return likeli



import pickle
import datetime
import pathlib
def saveCache(lossRecord, args):
    cachePath = args.path + '/cache'
    pathlib.Path(cachePath).mkdir(parents=True,exist_ok=True)
    with open(cachePath + '/' + 'lossRecord.pickle', 'wb') as f:
        pickle.dump(lossRecord, f)
    with open(cachePath + '/' + 'parameters.txt', 'wb') as f:
        pickle.dump(str(args), f)
    return cachePath + '/' + '.pickle'


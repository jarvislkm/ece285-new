from torchvision.utils import save_image
import numpy as np
import torch
import pathlib
from Utils.loadData import pca_decode

def sampling(number, target_one_hot, model, epoch, args):
    savePath = args.path + '/' + 'sampling'
    pathlib.Path(savePath).mkdir(parents=True,exist_ok=True)
    with torch.no_grad():
        target_one_hot = target_one_hot.to(args.device)
        sample = model.sampling(number, target_one_hot).to(args.device)
        if args.ifpca == True:
            decode = pca_decode(args.device, sample, args.mu, args.U)
        else:
            decode = sample
        decode_ = decode.view(number, args.inputSize[0], args.inputSize[1], args.inputSize[2])
        if args.cvae and (args.cvae_select == 'line'):
            target_insert = torch.tensor(np.reshape(target_one_hot,(number, args.inputSize[0], args.inputSize[1])))
            decode_[:,:,:,int(args.inputSize[2]/2)] = (target_insert+1)/2
        save_image(decode_, savePath+ '/' + str(epoch) + '.png')

def cvae_create_target_one_hot(args, number, target):
    target_one_hot = torch.zeros(number, args.conditional_dim)
    # for i in range(8):
    #     target_one_hot[8*i:8*(i+1), i] = 1
    for i in range(64):
        target_one_hot[i, np.random.randint(0,9)] = 1
    return target_one_hot

def cvae_create_target_line(args, number, test_loader):
    for batch_index, (data, labels) in enumerate(test_loader):
        target_one_hot = labels[0:number]
        return target_one_hot
import torch.nn as nn
import numpy as np
import torch.nn.functional as F
import torch
from itertools import product
from Utils.likelihood import likelihood_log_p_z, likelihood_log_q_z, binary_cross_entropy
from Utils.loadData import pca_decode

class vae(nn.Module):
    def __init__(self, args):
        super(vae,self).__init__()
        self.args = args
        size = np.prod(self.args.inputSize).item()
        internal_layer_dim = args.ILD

        if args.ifpca == True:
            size = min(size, args.pcadimension)
        self.inputlayerSize = size

        self.q1 = nn.Linear(size + self.args.conditional_dim, internal_layer_dim)    
        self.q2 = nn.Linear(internal_layer_dim, internal_layer_dim)

        self.q_mean = nn.Linear(internal_layer_dim, self.args.hiddenSize)
        self.q_logvar = nn.Linear(internal_layer_dim, self.args.hiddenSize)
        
        self.p1 = nn.Linear(self.args.hiddenSize + self.args.conditional_dim, internal_layer_dim)
        self.p2 = nn.Linear(internal_layer_dim, internal_layer_dim)

        self.p3 = nn.Linear(internal_layer_dim, size)
        self.init()

    def init(self):
        for layer in self.modules():
            if isinstance(layer, nn.Linear):
                nn.init.xavier_uniform_(layer.weight, gain=nn.init.calculate_gain('relu'))

    def encode(self, x, y):
        if self.args.cvae == True:
            code = F.relu(self.q2(F.relu(self.q1(torch.cat((x,y),1)))))
        else:
            code = F.relu(self.q2(F.relu(self.q1(x))))
        return self.q_mean(code), self.q_logvar(code)

    def reparameterization(self, mean, logvar):
        if self.training:
            std = torch.exp(0.5 * logvar)
            eps = torch.randn_like(std)
            return eps.mul(std).add_(mean)
        else:
            return mean

    def decode(self, z, y):
        if self.args.cvae == True:
            recon = F.relu(self.p2(F.relu(self.p1(torch.cat((z,y),1)))))
        else:
            recon = F.relu(self.p2(F.relu(self.p1(z))))

        if self.args.ifpca == True:
            return self.p3(recon) # 
        else:
            return F.sigmoid(self.p3(recon))

    def forward(self, x, y):
        mean, logvar = self.encode(x, y)
        z = self.reparameterization(mean, logvar)
        recon = self.decode(z, y)
        return z, mean, logvar, recon

    def loss(self, x, y, average = True):
        z, mean, logvar, recon = self.forward(x, y)
        loss, RE, KL = self.loss_cal(x, z, recon, mean, logvar, average)
        return loss, RE, KL, recon

    def loss_cal(self, x, z, recon, mean, logvar,average = False):
        #RE
        # RE = F.binary_cross_entropy(recon, x.view(-1, np.prod(self.args.inputSize)), size_average=False)        
        if (not average and self.args.ifpca) == True:
            real_recon = pca_decode(self.args.device, recon, self.args.mu, self.args.U)
            real_data = pca_decode(self.args.device, x.view(-1, self.inputlayerSize), self.args.mu, self.args.U)
            RE = -binary_cross_entropy(real_recon, real_data).to(self.args.device)

        elif self.args.ifpca == True:
            a = recon - x.view(-1, self.inputlayerSize)
            b = torch.mul(a,a)
            RE = torch.sum(b)/len(b)
        else:
            RE = -binary_cross_entropy(recon, x.view(-1, self.inputlayerSize))

        #KL
        #KL = -0.5 * torch.sum(1 + logvar - mean.pow(2) - logvar.exp(), 1)
        log_p_z = likelihood_log_p_z(z)
        log_q_z = likelihood_log_q_z(z, mean, logvar)
        KL = log_q_z - log_p_z
        if average:
            RE = torch.mean(RE, 0)
            KL = torch.mean(KL, 0)

        loss = RE + self.args.KLratio * KL
        return loss, RE, KL

    def sampling(self, number, target_one_hot, repeat = 8):
        if False:
            sample = torch.randn(int(number/repeat), self.args.hiddenSize).to(self.args.device)
            sample = sample.repeat(repeat, 1)
        else:
            sample = torch.randn(number, self.args.hiddenSize).to(self.args.device)
        sample = self.decode(sample, target_one_hot).cpu()
        return sample

    def unisampling(self,number):
        # run when there are only two latent variables
        # uniform sampling
        perm = torch.FloatTensor(np.array(list(product(np.linspace(-1.5,1.5,number), np.linspace(-1.5,1.5,number)))))
        z2 = perm.to(self.args.device)
        y = torch.tensor([])
        sample = self.decode(z2, y).cpu()
        return sample






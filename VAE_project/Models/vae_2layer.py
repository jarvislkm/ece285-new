import torch.nn as nn
import numpy as np
import torch.nn.functional as F
import torch
from itertools import product
from Utils.likelihood import likelihood_log_p_z, likelihood_log_q_z, binary_cross_entropy
from Utils.loadData import pca_decode

class vae(nn.Module):
    def __init__(self, args):
        super(vae,self).__init__()
        self.args = args
        size = np.prod(self.args.inputSize).item()
        internal_layer_dim = args.ILD

        if args.ifpca == True:
            size = min(size, args.pcadimension)
        self.inputlayerSize = size
        # encode z2|x
        self.q_a1 = nn.Linear(size + self.args.conditional_dim, internal_layer_dim)
        self.q_a2 = nn.Linear(internal_layer_dim, internal_layer_dim)

        self.q_z2_mean = nn.Linear(internal_layer_dim, self.args.hiddenSize)
        self.q_z2_logvar = nn.Linear(internal_layer_dim, self.args.hiddenSize)
        # encode z1|x,z2
        self.q_b1z = nn.Linear(self.args.hiddenSize, internal_layer_dim)
        self.q_b1x = nn.Linear(size, internal_layer_dim)
        self.q_comb = nn.Linear(internal_layer_dim*2, internal_layer_dim)

        self.q_z1_mean = nn.Linear(internal_layer_dim, self.args.hiddenSize)
        self.q_z1_logvar = nn.Linear(internal_layer_dim, self.args.hiddenSize)
        
        # decode z1|z2
        self.p_z1_1 = nn.Linear(self.args.hiddenSize + self.args.conditional_dim, internal_layer_dim)
        self.p_z1_2 = nn.Linear(internal_layer_dim, internal_layer_dim)

        self.p_z1_mean = nn.Linear(internal_layer_dim, self.args.hiddenSize)
        self.p_z1_logvar = nn.Linear(internal_layer_dim, self.args.hiddenSize)
        # decode x|z1,z2
        self.p1a = nn.Linear(self.args.hiddenSize, internal_layer_dim)
        self.p1b = nn.Linear(self.args.hiddenSize, internal_layer_dim)
        self.p2 = nn.Linear(internal_layer_dim*2, internal_layer_dim)
        self.p3 = nn.Linear(internal_layer_dim, size)
        self.init()

    def init(self):
        for layer in self.modules():
            if isinstance(layer, nn.Linear):
                nn.init.xavier_uniform_(layer.weight, gain=nn.init.calculate_gain('relu'))

    def encode_z2(self, x, y):
        if self.args.cvae == True:
            code = F.relu(self.q_a2(F.relu(self.q_a1(torch.cat((x,y),1)))))
        else:
            code = F.relu(self.q_a2(F.relu(self.q_a1(x))))
        return self.q_z2_mean(code), self.q_z2_logvar(code)

    def encode_z1(self, x, z2):
        b1x = F.relu(self.q_b1x(x))
        b1z = F.relu(self.q_b1z(z2))
        b1 = self.q_comb(torch.cat((b1x, b1z), 1))
        return self.q_z1_mean(b1), self.q_z1_logvar(b1)

    def reparameterization(self, mean, logvar):
        if self.training:
            std = torch.exp(0.5 * logvar)
            eps = torch.randn_like(std)
            return eps.mul(std).add_(mean)
        else:
            return mean

    def decode_z1(self, z2, y):
        if self.args.cvae == True:
            p_z1 = F.relu(self.p_z1_2(F.relu(self.p_z1_1(torch.cat((z2,y),1)))))
        else:
            p_z1 = F.relu(self.p_z1_2(F.relu(self.p_z1_1(z2))))
        z1_mean = self.p_z1_mean(p_z1)
        z1_logvar = self.p_z1_logvar(p_z1)
        return z1_mean, z1_logvar

    def decode_x(self, z1, z2):
        p1a = F.relu((self.p1a(z1)))
        p1b = F.relu((self.p1b(z2)))
        p1_tmp = torch.cat((p1a, p1b), 1)
        p2_tmp = F.relu(self.p2(p1_tmp))
        if self.args.ifpca == True:
            return self.p3(p2_tmp) # 
        else:
            return F.sigmoid(self.p3(p2_tmp))

    def forward(self, x, y):
        # encode z2|x
        z2_mean, z2_logvar = self.encode_z2(x, y)
        z2 = self.reparameterization(z2_mean, z2_logvar)
        # encode z1|x, z2
        z1_mean, z1_logvar = self.encode_z1(x, z2)
        z1 = self.reparameterization(z1_mean, z1_logvar)
        # encode 
        pz1_mean, pz1_logvar = self.decode_z1(z2, y)

        recon = self.decode_x(z1, z2)
        return recon, z1, z2, z2_mean, z2_logvar, z1_mean, z1_logvar, pz1_mean, pz1_logvar
    
    def loss(self, x, y, average = True):
        recon, z1, z2, z2_mean, z2_logvar, z1_mean, z1_logvar, pz1_mean, pz1_logvar = self.forward(x, y)
        loss, RE, KL = self.loss_cal(x, recon, z1, z2, z2_mean, z2_logvar, z1_mean, z1_logvar, pz1_mean, pz1_logvar,average)
        return loss, RE, KL, recon

    def loss_cal(self, x, recon, z1, z2, z2_mean, z2_logvar, z1_mean, z1_logvar, pz1_mean, pz1_logvar,average=False):
        # RE
        if (not average and self.args.ifpca) == True:
            real_recon = pca_decode(self.args.device, recon, self.args.mu, self.args.U)
            real_data = pca_decode(self.args.device, x.view(-1, self.inputlayerSize), self.args.mu, self.args.U)
            RE = -binary_cross_entropy(real_recon, real_data).to(self.args.device)

        elif self.args.ifpca == True:
            a = recon - x.view(-1, self.inputlayerSize)
            b = torch.mul(a,a)
            RE = torch.sum(b)/len(b)
        else:
            RE = -binary_cross_entropy(recon, x.view(-1, self.inputlayerSize))
        # KL
        # p(z2) ~ N(0,I) true distribution of hidden unit is not influenced by input x.
        log_p_z2 = likelihood_log_p_z(z2)
        # q(z2|x) ~ N(u, var) Given certain x, the hidden representation is Normal distribution.
        log_q_z2 = likelihood_log_q_z(z2, z2_mean, z2_logvar)
        # p(z1|z2) ~ N(u, var)
        log_p_z1 = likelihood_log_q_z(z1, pz1_mean, pz1_logvar)
        # q(z1|z2,x) ~ N(u, var)
        log_q_z1 = likelihood_log_q_z(z1, z1_mean, z1_logvar)

        KL = log_q_z1 + log_q_z2 - log_p_z1 - log_p_z2
        if average:
            RE = torch.mean(RE, 0)
            KL = torch.mean(KL, 0)
        loss = RE + KL
        return loss, RE, KL
    
    def sampling(self, number, target_one_hot, repeat = 8):
        # z2 ~ N(0,I)
        if self.args.cvae:
            sample = torch.randn(int(number/repeat), self.args.hiddenSize).to(self.args.device)
            z2 = sample.repeat(repeat, 1)
        else:
            z2 = torch.randn(number, self.args.hiddenSize).to(self.args.device)

        # z1 ~ p(z1|z2)
        z1_mean, z1_logvar = self.decode_z1(z2, target_one_hot)
        z1 = self.reparameterization(z1_mean, z1_logvar)
        sample = self.decode_x(z1,z2).cpu()
        return sample

    def unisampling(self,number):
        # run when there are only two latent variables
        self.eval()
        # z2 ~ uniform distribution
        perm = torch.FloatTensor(np.array(list(product(np.linspace(-1.5,1.5,number), np.linspace(-1.5,1.5,number)))))
        z2 = perm.to(self.args.device)
        # z1 ~ p(z1|z2)
        y = torch.tensor([])
        z1_mean, z1_logvar = self.decode_z1(z2, y)
        z1 = self.reparameterization(z1_mean, z1_logvar)
        sample = self.decode_x(z1,z2).cpu()
        return sample








